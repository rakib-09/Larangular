import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { QuotesService } from '../quotes.service';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {

  constructor(private quoteservice: QuotesService ) { }

  ngOnInit() {
  }
  onSubmit(form: NgForm) {
    this.quoteservice.addQuote(form.value.content)
    .subscribe(
      () => alert('Quotes Created!'),
    );

    form.reset();
  }

}
