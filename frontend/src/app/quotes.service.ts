import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class QuotesService {

  constructor(private _http: Http) { }

  getQuotes(): Observable<any> {
    return this._http.get('http://localhost/deploy/api/showdata')
    .map(
      (response: Response) => {
        return response.json().quotes;
      }
    );
  }

  addQuote(content: string) {
    const body = JSON.stringify({content: content});
    const headers = new Headers({'Content-Type': 'application/json'});
    return this._http.post('http://localhost/deploy/api/store', body, {headers: headers});
  }

}
