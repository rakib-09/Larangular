import { Component, OnInit } from '@angular/core';
import { Quote } from '../quote';
import { QuotesService } from '../quotes.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  quotes: Quote[];
  constructor(private quoteservice: QuotesService) { }

  ngOnInit() {
  }
  onGetQuotes() {
    this.quoteservice.getQuotes()
    .subscribe(
      (quotes: Quote[]) => this.quotes = quotes,
      (error: Response) => console.log(error)
    );
  }
}
